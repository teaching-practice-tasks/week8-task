import os
import unittest
from collections import defaultdict

from solution import word_count


class TestWordCount(unittest.TestCase):
    def setUp(self):
        self.file_contents = [
            "Hello World! How are you?",
            "Hello World! How are you? Hello again!",
            "Hello World! How, are you? Hello, again!",
            "Hello World! How, are you? Hello, again! This is a test.",
            "This is a long sentence with many words. It is used to test the word count function ability to handle "
            "large inputs and different sentence structures."
        ]

    def tearDown(self):
        for file_path in ["sample1.txt", "sample2.txt", "sample3.txt", "sample4.txt", "sample5.txt"]:
            if os.path.exists(file_path):
                os.remove(file_path)

    def test_1(self):
        file_path = "sample1.txt"
        with open(file_path, "w") as file:
            file.write(self.file_contents[0])
        result = word_count(file_path)
        self.assertDictEqual(result, defaultdict(int, {'hello': 1, 'world': 1, 'how': 1, 'are': 1, 'you': 1}))

    def test_2(self):
        file_path = "sample2.txt"
        with open(file_path, "w") as file:
            file.write(self.file_contents[1])
        result = word_count(file_path)
        self.assertDictEqual(result,
                             defaultdict(int, {'hello': 2, 'world': 1, 'how': 1, 'are': 1, 'you': 1, 'again': 1}))

    def test_3(self):
        file_path = "sample3.txt"
        with open(file_path, "w") as file:
            file.write(self.file_contents[2])
        result = word_count(file_path)
        self.assertDictEqual(result,
                             defaultdict(int, {'hello': 2, 'world': 1, 'how': 1, 'are': 1, 'you': 1, 'again': 1}))

    def test_4(self):
        file_path = "sample4.txt"
        with open(file_path, "w") as file:
            file.write(self.file_contents[3])
        result = word_count(file_path)
        self.assertDictEqual(result, defaultdict(int, {'hello': 2, 'world': 1, 'how': 1, 'are': 1, 'you': 1, 'again': 1,
                                                       'this': 1, 'is': 1, 'a': 1, 'test': 1}))

    def test_5(self):
        file_path = "sample5.txt"
        with open(file_path, "w") as file:
            file.write(self.file_contents[4])
        result = word_count(file_path)
        self.assertDictEqual(result, defaultdict(int, {'this': 1, 'is': 2, 'a': 1, 'long': 1, 'sentence': 2, 'with': 1,
                                                       'many': 1, 'words': 1, 'it': 1, 'used': 1, 'to': 2, 'test': 1,
                                                       'the': 1, 'word': 1, 'count': 1, 'function': 1, 'ability': 1,
                                                       'handle': 1, 'large': 1, 'inputs': 1, 'and': 1, 'different': 1,
                                                       'structures': 1}))
