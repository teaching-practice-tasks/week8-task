### Week 8: Files

## Task: File Word Count

Write a Python function `word_count(file_path: str) -> Dict[str, int]` that takes a file path as input and returns
a dictionary where keys are words found in the file and values are the number of times each word appears in the file.
The function should ignore punctuation marks and convert all words to lowercase.

### Input

A string `file_path` representing the path to a text file.

### Output

A dictionary of words as keys and their frequency as values.

### Example

```
>>> word_count("sample.txt")
{'hello': 2, 'world': 1, 'how': 1, 'are': 1, 'you': 1}
```

You have to implement function `word_count(file_path: str) -> Dict[str, int]` in `solution.py` file.
You may create additional functions